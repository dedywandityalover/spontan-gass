<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
// route dashboard
$routes->get('/dashboard', 'dashboard::index');

$routes->get('/', 'Forum::index');

$routes->post('/forum/ask', 'Forum::ask');

$routes->get('/register', 'auth::register_index');
$routes->post('auth/register', 'auth::register');

$routes->get('/login', 'auth::login_index');
$routes->post('auth/login', 'auth::login');



// Route Form
$routes->get('/form', 'Form::index');
$routes->post('/form/tambah', 'Form::tambah');



