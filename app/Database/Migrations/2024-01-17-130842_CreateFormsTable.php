<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateFormsTable extends Migration
{
    public function up()
{
    $this->forge->addField([
        'id' => [
            'type' => 'INT',
            'constraint' => 5,
            'unsigned' => true,
            'auto_increment' => true,
        ],
        
        'jenis_kelamin' => [
            'type' => 'ENUM',
            'constraint' => ['Laki-laki', 'Perempuan'],
        ],

        'umur' => [
            'type' => 'VARCHAR',
            'constraint' => '255',
        ],

        'tinggi_badan' => [
            'type' => 'VARCHAR',
            'constraint' => '255',
        ],

        'berat_badan_user' => [
            'type' => 'VARCHAR',
            'constraint' => '255',
        ],
        'berat_badan_target' => [
            'type' => 'VARCHAR',
            'constraint' => '255',
        ],

        'ukuran_badan_user' => [
            'type' => 'VARCHAR',
            'constraint' => '255',
        ],

        'ukuran_badan_target' => [
            'type' => 'VARCHAR',
            'constraint' => '255',
        ],

        'waktu_olahraga' => [
            'type' => 'VARCHAR',
            'constraint' => '225',
        ],
    ]);

    $this->forge->addKey('id', true);
    $this->forge->createTable('forms');
}

    public function down()
    {
        $this->forge->dropTable('forms');
    }
}
