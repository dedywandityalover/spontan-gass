<?php

namespace App\Models;

use CodeIgniter\Model;

class FormModel extends Model
{

    protected $primaryKey = 'id';
    protected $table = 'Forms';
    protected $allowedFields = ['jenis_kelamin', 'umur', 'tinggi_badan', 'berat_badan_user', 'berat_badan_target',
                                'ukuran_badan_user', 'ukuran_badan_target', 'waktu_olahraga'];
}

