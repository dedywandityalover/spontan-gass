<?php

namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{

    protected $primaryKey = 'id';
    protected $table = 'users';
    protected $allowedFields = ['username', 'email', 'password'];
}

// function save_data($arrSave)
// {
//     $this->builder->insert($arrSave);

//     return $this->db->insertID;
// }

