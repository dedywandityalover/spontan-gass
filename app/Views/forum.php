<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Simple Forum</title>
</head>
<body>
    <h1>Simple Forum</h1>
    <textarea id="question" placeholder="Tanyakan sesuatu..."></textarea>
    <button onclick="askQuestion()">Tanyakan</button>
    <p id="answer"></p>

    <script>
        async function askQuestion() {
            const question = document.getElementById('question').value;

            const response = await fetch('/forum/ask', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ question }),
            });

            const data = await response.json();
            document.getElementById('answer').innerText = data.answer;
        }
    </script>
</body>
</html>
