<!-- App/Views/register.php -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrasi</title>
</head>
<body>

    <h2>Registrasi Pengguna</h2>

    <!-- Form Registrasi -->
    <?= form_open('/auth/register') ?>
        <label for="username">Username:</label>
        <input type="text" name="username" required>
        <br>

        <label for="email">Email:</label>
        <input type="email" name="email" required>
        <br>

        <label for="password">Password:</label>
        <input type="password" name="password" required>
        <br>

        <button type="submit">Registrasi</button>
    <?= form_close() ?>

</body>
</html>
