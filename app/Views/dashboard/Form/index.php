<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
    <style>
    form {
      max-width: 400px;
      margin: auto;
    }

    label {
      margin-bottom: 8px;
    }

    input, select {
      width: 100%;
      padding: 8px;
      margin-bottom: 16px;
      box-sizing: border-box;
    }

    button {
      background-color: #4CAF50;
      color: white;
      padding: 10px;
      border: none;
      cursor: pointer;
    }
  </style>
</head>
<body>

    <h2>Form</h2>

    <!-- Form Registrasi -->
    <?= form_open('/form/tambah') ?>
    <label for="jenis_kelamin">Jenis Kelamin:</label>
    <select id="jenis_kelamin" name="jenis_kelamin">
      <option value="Laki-laki">Laki-laki</option>
      <option value="Perempuan">Perempuan</option>
    </select>

    <label for="umur">Umur:</label>
    <input type="number" id="umur" name="umur" min="1" required>

    <label for="tinggi_badan">Tinggi Badan (cm):</label>
    <input type="number" id="height" name="tinggi_badan" min="1" required>
    
    <label for="berat_badan_user">Berat Badan (cm):</label>
    <input type="number" id="berat_badan_user" name="berat_badan_user" min="1" required>

    <label for="berat_badan_target">Berat Badan Target (kg):</label>
    <input type="number" id="berat_badan_target" name="berat_badan_target" min="1" required>

    <label for="ukuran_badan_user">Ukuran Badan (kg):</label>
    <input type="text" id="ukuran_badan_user" name="ukuran_badan_user" min="1" required>
    
    <label for="ukuran_badan_target">Ukuran Badan Target (kg):</label>
    <input type="text" id="ukuran_badan_target" name="ukuran_badan_target" min="1" required>

    <label for="waktu_olahraga">Waktu Olahraga ?</label>
        <input type="range" id="waktu_olahraga" name="waktu_olahraga" min="1" max="5" oninput="updateValue(this.value)">
        <span id="sliderValue">Nilai: </span>
     <br>
     <br>

    <button type="submit">Submit</button>
    <?= form_close() ?>

    <script>
        function updateValue(value) {
            document.getElementById('sliderValue').innerText = 'Nilai: ' + value;
        }
    </script>

</body>
</html>