<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\Filters\FilterInterface;
use App\Models\UserModel;


class Auth extends BaseController
{

     public function register_index()
    {
         return view('registrasi');
    }

   public function register()
{
    // $validation = \Config\Services::validation();
    // // Validasi input form (bisa menggunakan form validation)
    // $rules = [
    //     'username' => 'required|min_length[5]|max_length[255]',
    //     'email' => 'required|valid_email',
    //     'password' => 'required|min_length[6]',
    // ];

    // if (!$validation->run($this->request->getPost(), null, null, $rules)) {
    //     // Tangani kesalahan validasi
    //     return redirect()->to('/register')->withInput()->with('errors', $validation->getErrors());
    // } else {
        // Simpan data pengguna ke database
        $userModel = new UserModel();
        $userData = [
            'id'       => $this->request->getPost('id'),
            'username' => $this->request->getPost('username'),
            'email'    => $this->request->getPost('email'),
            'password' => password_hash($this->request->getPost('password'), PASSWORD_DEFAULT),
        ];

        // Menyimpan data pengguna ke database
        $userModel->save($userData);
    

       

        return redirect()->to('/login')->with('success', 'Registrasi berhasil! Silakan login.');
    
}
    
    

    public function login_index()
    {
        return view('login');
    }

    // {

    //     // Simpan data pengguna ke database
    //     $userModel = new \App\Models\UserModel();
    //     $userModel->save([
    //         'username' => $this->request->getPost('username'),
    //         'email'    => $this->request->getPost('email'),
    //         'password' => password_hash($this->request->getPost('password'), PASSWORD_DEFAULT),
    //     ]);

    //     return redirect()->to('/login')->with('success', 'Registrasi berhasil! Silakan login.');
    // }

     public function login()
    {
        // Validasi input form (bisa menggunakan form validation)
      

        $username = $this->request->getPost('username');
        $password = $this->request->getPost('password');

        // Proses autentikasi, sesuaikan dengan kebutuhan Anda
        // Misalnya, Anda dapat menggunakan model untuk memeriksa keberadaan pengguna di database

        // Jika autentikasi berhasil, arahkan ke halaman utama
        return redirect()->to('/dashboard')->with('success', 'Login berhasil!');
        
        // Jika autentikasi gagal, arahkan kembali ke halaman login dengan pesan kesalahan
        return redirect()->back()->withInput()->with('error', 'Login gagal. Cek kembali username dan password Anda.');
    }

    // class Dashboard extends BaseController implements FilterInterface
    // {
    // public function before(RequestInterface $request, $arguments = null)
    //     {
    //     // Validasi apakah pengguna sudah login atau belum
    //     if (!session()->get('isLoggedIn')) {
    //         return redirect()->to('/login'); // Ganti dengan URL login sesuai kebutuhan
    //         }
    //     }
    // }
}
