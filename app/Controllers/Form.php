<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\Filters\FilterInterface;
use App\Models\FormModel;


class Form extends BaseController
{
    public function index()
    {
         return view('dashboard/form/index');
    } 
    
    public function tambah()
    {
        $FormModel = new FormModel();
        $FormUser = [
            'id'            => $this->request->getPost('id'),
            'jenis_kelamin' => $this->request->getPost('jenis_kelamin'),
            'umur'          => $this->request->getPost('umur'),
            'tinggi_badan'  => $this->request->getPost('tinggi_badan'),
            'berat_badan_user'  => $this->request->getPost('berat_badan_user'),
            'berat_badan_target'  => $this->request->getPost('berat_badan_target'),
            'ukuran_badan_user'  => $this->request->getPost('ukuran_badan_user'),
            'ukuran_badan_target'  => $this->request->getPost('ukuran_badan_target'),
            'berat_badan_user'  => $this->request->getPost('berat_badan_user'),
            'berat_badan_user'  => $this->request->getPost('berat_badan_user'),
            'waktu_olahraga'  => $this->request->getPost('waktu_olahraga'),
           
            
        ];

        // Menyimpan data pengguna ke database
        $FormModel->save($FormUser);

        return view('dashboard/index');
    }

}