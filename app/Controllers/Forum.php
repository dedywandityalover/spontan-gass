<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;

class Forum extends BaseController
{
    public function index()
    {
         return view('forum');
    }

    public function ask()
    {
        $question = $this->request->getPost('question');
        // Implementasi logika untuk menghasilkan jawaban berdasarkan pertanyaan
        $answer = $this->generateAnswer($question);
        return $this->response->setJSON(['answer' => $answer]);
    }

    private function generateAnswer($question)
    {
        // Implementasi logika untuk menghasilkan jawaban
        return "Ini jawaban statis untuk pertanyaan Anda.";
    }
}
